Name: Shashank Harinath
USC ID: 1438401448
URL: http://www.sqlfiddle.com/#!2/d90641/2

set @point = GeomFromText('POINT(1 1)');
set @polygon = GeomFromText('POLYGON((0 0,3 0,0 3,0 0))');

select GeometryType(@polygon);
select Within(@point, @polygon);
select Contains(@polygon, @point);
select AsText(@polygon);
select AsText(Centroid(@polygon));