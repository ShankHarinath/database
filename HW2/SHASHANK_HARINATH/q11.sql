select distinct v.owner_id, u.USER_FIRST_NAME as owner_first_name, u.USER_LAST_NAME as owner_last_name from 
(Select villa_id, sum(reservation_end_date - reservation_start_date) as reservation_days_2014 from reservation where reservation_start_date < to_date('31-08-2014','DD-MM-YYYY') 
and reservation_end_date > to_date('01-01-2014','DD-MM-YYYY') group by villa_id) T14 , 
(Select villa_id, sum(reservation_end_date - reservation_start_date) as reservation_days_2013 from reservation where reservation_start_date < to_date('31-08-2013','DD-MM-YYYY') 
and reservation_end_date > to_date('01-01-2013','DD-MM-YYYY') group by villa_id) T13 ,
VILLA V,
User_item u
where T14.villa_id = t13.villa_id and V.villa_id = t13.villa_id and v.owner_id = u.user_id
and ((reservation_days_2014-reservation_days_2013)/reservation_days_2013)*100 > 10;