select u.user_id as owner_id, u.user_first_name as Owner_first_name, u.user_last_name as Owner_last_name, count(v.villa_id) as VILLA_COUNT from villa v, user_item u 
where (v.owner_id = u.user_id) 
group by u.user_first_name, u.user_last_name, u.user_id order by count(v.villa_id) desc;