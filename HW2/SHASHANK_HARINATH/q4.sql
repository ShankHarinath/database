select * from 
(select u.user_id, u.user_first_name, u.user_last_name, sum(r.reservation_deposit)
from reservation r, user_item u where (u.user_id = r.user_id) and r.RESERVATION_START_DATE <= to_date('12-31-2013', 'MM-DD-YYYY')
and r.RESERVATION_START_DATE > to_date('12-31-2012', 'MM-DD-YYYY') 
group by u.user_id, u.user_first_name, u.user_last_name
order by sum(r.reservation_deposit) desc)
where ROWNUM <= 3;