select v.villa_id,v.villa_name, u.user_id as owner_id,u.user_first_name as owner_first_name, u.user_last_name as owner_last_name
from villa v, user_item u 
where
v.owner_id = u.user_id and villa_id in (
select villa_id from   (
select villa_id, avg(rating) 
from review
group by villa_id
order by avg(rating)  desc
)
where rownum<=3);