select * from 
(select r.villa_id, v.villa_name, sum(r.RESERVATION_end_DATE- r.RESERVATION_START_DATE) as Num_of_res
from reservation r, villa v 
where r.RESERVATION_START_DATE between TO_DATE('01-01-2014','MM-DD-YYYY') AND TO_DATE('12-31-2014','MM-DD-YYYY') 
and r.VILLA_ID = v.VILLA_ID
group by r.villa_id, v.villa_name 
order by sum(r.RESERVATION_end_DATE- r.RESERVATION_START_DATE) desc) where rownum = 1;