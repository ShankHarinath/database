select u.user_id, u.user_first_name, u.user_last_name from
(select r1.user_id, r1.villa_id, r2.max_likes
from
(select r.user_id, r.villa_id, count(*) as likes
from review r, review_details l
where r.review_id = l.review_id
group by r.user_id, r.villa_id) r1,
(select villa_id, max(likes) as max_likes
from 
(select r.user_id, r.villa_id, count(*) as likes
from review r, review_details l
where r.review_id = l.review_id
group by r.user_id,r.villa_id)
group by villa_id) r2
where
r1.likes = r2.max_likes and r1.villa_id = r2.villa_id) q , user_item u 
where q.user_id = u.USER_ID and rownum=1;