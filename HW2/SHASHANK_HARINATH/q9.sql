Select u.user_first_name as owner_first_name, u.USER_LAST_NAME as owner_last_name, Q.* from (Select B.owner_id, decode((B.TOTAL_COUNT-D.VACANT_VILLA_COUNT)/B.TOTAL_COUNT, null, 1, (B.TOTAL_COUNT-D.VACANT_VILLA_COUNT)/B.TOTAL_COUNT) as Vacancy_Ratio
from 
(select count(*) as TOTAL_COUNT, owner_id from villa group by owner_id) B 
left outer join 
(select v.owner_id, count(*) as VACANT_VILLA_COUNT from reservation r, villa v where r.VILLA_ID = v.VILLA_ID 
and (ABS((to_date('08-15-2014','MM-DD-YYYY') - r.RESERVATION_START_DATE)) < ABS(r.RESERVATION_end_DATE - r.RESERVATION_start_DATE)) 
group by v.owner_id) D
on 
B.owner_id = D.owner_id
order by B.owner_id) Q, user_item u where Q.owner_id = u.user_id;