<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="/">
		<html>
			<style type="text/css">
     			 body { font-family:Arial; font-size:12px}
    		</style>
			<body>
				<h2
					style="font-size:24px; background-color:Yellow; font-weight:bold; color:red">Employee Information</h2>
				<xsl:for-each select="company/employees/employee">
					<p>
						Employee
						<strong><xsl:value-of select="empName" /></strong>
						works from
						<strong><xsl:value-of select="empOffice" /></strong>
						office.
						<strong><xsl:value-of select="empName" /></strong>
						works for
						<strong><xsl:value-of
							select="count(/company/worksForDept/worksFor[empId=current()/empId])" /></strong>
						division(s), which are
						<xsl:for-each
							select="/company/worksForDept/worksFor[empId=current()/empId]">
							<strong><xsl:value-of
								select="/company/divisions/division[divId=current()/divId]/divName" /></strong>
							<xsl:choose>
								<xsl:when test="position() = last()-1"><strong> and </strong></xsl:when>
  								<xsl:when test="(position() != last())"><strong>, </strong></xsl:when>
  							</xsl:choose>
						</xsl:for-each>.

						<strong><xsl:value-of select="empName" /></strong>
						manages
						<strong><xsl:value-of
							select="count(/company/divisions/division[managerEmpId=current()/empId])" /></strong>
						<xsl:variable name="child" select="/company/divisions/division[managerEmpId=current()/empId]"/>
        				<xsl:if test="count($child) > 0"> division(s), which are </xsl:if>
        				<xsl:if test="count($child) = 0"> division(s)</xsl:if>
						<xsl:for-each
							select="/company/divisions/division[managerEmpId=current()/empId]">
							<strong><xsl:value-of select="current()/divName" /></strong>
							<xsl:choose>
								<xsl:when test="position() = last()-1"><strong> and </strong></xsl:when>
  								<xsl:when test="(position() != last())"><strong>, </strong></xsl:when>
  							</xsl:choose>
						</xsl:for-each>.
						
						<strong><xsl:value-of select="empName" /></strong>
						works for the most time with the
						
						<xsl:variable name="max">
     						<xsl:for-each select="/company/worksForDept/worksFor[empId=current()/empId]">
       							<xsl:sort select="current()/percentTime" data-type="number" order="descending"/>
       							<xsl:if test="position()=1"><strong><xsl:value-of select="current()/divId"/></strong></xsl:if>
     						</xsl:for-each>
   						</xsl:variable>
   						<strong><xsl:value-of select="/company/divisions/division[divId=$max]/divName" /></strong> division.
					</p>
				</xsl:for-each>
				
				<h2 style='font-size:24px;background-color:LightGreen;font-weight:bold;color:red'>Department Information</h2>
				<xsl:for-each select="/company/departments/department">
					<p>
						Department <strong><xsl:value-of select="deptName" /> </strong>
						houses
						<strong><xsl:value-of
								select="count(/company/divisions/division[housedDeptId=current()/deptId])" /></strong>
						division(s):
						<xsl:for-each
							select="/company/divisions/division[housedDeptId=current()/deptId]">
							<strong><xsl:value-of select="current()/divName" /></strong>
							<xsl:choose>
								<xsl:when test="position() = last()-1"><strong> and </strong></xsl:when>
								<xsl:when test="(position() != last())"><strong>, </strong></xsl:when>
							</xsl:choose>
						</xsl:for-each>.
					</p>
				</xsl:for-each>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>

