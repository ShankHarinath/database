let $doc := doc("company.xml")
let $Jid := $doc/company/employees/employee[empName="Jack"]/empId
let $PSid := $doc/company/employees/employee[empName="PSmith"]/empId
let $sol :=
(for $x in $doc/company/worksForDept/worksFor
let $div := $doc/company/divisions/division[divId=$x/divId]
where ($x[empId=$PSid] or $x[empId=$Jid]) and $x[percentTime >=50]
return <DepartmentName>{data($doc/company/departments/department[deptId=$div/housedDeptId]/deptName)}</DepartmentName>)
return <query1>{$sol}</query1>