let $doc := doc("company.xml")
let $empcount := count($doc/company/employees/employee)
let $sol :=
(for $div in $doc/company/divisions/division
let $divCount := count($doc/company/worksForDept/worksFor[divId=$div/divId])
return if ($divCount =  $empcount) then <DivisionName>{data($div/divName)}</DivisionName> else "")
return <query2>{$sol}</query2>