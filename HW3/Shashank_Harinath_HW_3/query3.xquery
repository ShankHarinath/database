let $doc := doc("company.xml")
let $PSid := $doc/company/employees/employee[empName = "PSmith"]/empId
let $Wid := $doc/company/employees/employee[empName = "Wong"]/empId
let $sol := 
(for $result in distinct-values ( for  $x in $doc/company/divisions/division[managerEmpId = $PSid]/housedDeptId return $x )
where $result != ( for  $y in $doc/company/divisions/division[managerEmpId = $Wid]/housedDeptId return $y ) 
return <DepartmentName> {data($doc/company/departments/department[deptId = $result]/deptName)}</DepartmentName> )
return <query3>{$sol}</query3>