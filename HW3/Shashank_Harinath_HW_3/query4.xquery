let $doc := doc("company.xml")
let $manager := distinct-values($doc/company/divisions/division/managerEmpId)
let $emp := $doc/company/employees/employee/empId
let $empIds := distinct-values($emp[not(.=$manager)])
let $sol :=
(for $x in $doc/company/employees/employee[empId =  $empIds]
order by $x/empName descending
return <employee><empName>{data($x/empName)}</empName><empPhone>{data($x/empPhone)}</empPhone><empOffice>{data($x/empOffice)}</empOffice></employee>)
return <query4>{$sol}</query4>
