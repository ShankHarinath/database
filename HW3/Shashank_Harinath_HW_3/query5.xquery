let $doc := doc("company.xml")
let $empcount := count($doc/company/employees/employee)
let $worksForCount := count ($doc/company/worksForDept/worksFor)
let $average := $worksForCount div $empcount
return <query5>{$average}</query5>