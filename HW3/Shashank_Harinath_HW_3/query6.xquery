let $doc := doc("company.xml")
let $emp :=
(for $empId in distinct-values($doc/company/worksForDept/worksFor/empId)
let $entries := $doc/company/worksForDept/worksFor[data(empId) = $empId ]
order by count($entries) descending
return $entries/empId)[1]

let $empName := $doc/company/employees/employee[empId=$emp]/empName
let $empPhone := $doc/company/employees/employee[empId=$emp]/empPhone
let $empOffice := $doc/company/employees/employee[empId=$emp]/empOffice

return <query6><empName>{data($empName)}</empName><empPhone>{data($empPhone)}</empPhone><empOffice>{data($empOffice)}</empOffice></query6>